# Manifest
## Context, problem
> We take seriously the existential danger imposed by misinformation,

including but not limited to fake news and extremist causes. 
Misinformation can be organized (including state-sponsored) or be unintended consequences of the mechanics of social networks.

> We understand the importance of enlightenment and the power of educated judgment.

> We understand that this skill can be learned and can be taught.

## Goals, governance 
> This is a movement to defend knowledge against misinformation. 

We believe that uncovering truth will ultimately serve humanity's best interests.

We do not serve one person or party's agenda. Hiding, altering or occluding truth can be beneficial to a group for a limited time, but systems, organizations and followers created to facilitate this will be damaging on larger scales.

> We harness the power of exponentials.

With time a large portion of people can be introduced to this movement.

Once a critical mass is achieved, most people can benefit directly or indirectly.

> We respect differences in people's political, religious, and cultural values.

 We do not try to impose one ideology on all. We aim to help people be equipped to make more educated decisions in their own frame of thought.

> No product can be perfect forever. We rely on evolution to avoid petrification of principles and implementations, and eventually becoming another ideology of the past.

> We believe that most people prefer to act altruistically,

unless there is a strong contrast with their needs (immediate or long-term, in all levels of Mazlo's pyramid), like security, prosperity, and group identity.

> We expect that organized attacks might be planned to alter the integrity of such a system. 

Early attempts should be made to provide proactive and retroactive counter-measures to such attacks. Scientific and technological machinery can greatly streamline these measures.

> The information should be available publicly without restriction. People can engage with this system at different levels.

Users can share content in their discussions. It should be easy for them to contribute to the system by answering simple multiple-choice questions. 
At different stages of the involvement, members can be encouraged to invite more people to grow the system. Users can also be encouraged to increase their involvement. Enough content should be created so that people can learn to identify fallacies, biases and make better judgements.

> We depend on transparency, governance by people, and the adoption of new technologies (possibly blockchain),

to enforce these as firmly as reasonably possible, while maintaining the ability for people to decide to evolve almost everything.

---

This is version 0.0.1 of the manifest. All versions prior to 1.0.0 are considered as draft.
